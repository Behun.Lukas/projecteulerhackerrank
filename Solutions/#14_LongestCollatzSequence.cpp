#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void countCollatz(vector<unsigned long long> &collatzValues, const unsigned long long &n)
{
    unsigned long long n_cur = n;
    unsigned long long count = 0;
    vector<unsigned long long> updatedIndices;

    while (n_cur != 1)
    {
        if (n_cur < collatzValues.size() && collatzValues[n_cur] != 0)
        {
            count += collatzValues[n_cur];
            break;
        }

        updatedIndices.push_back(n_cur);

        if (n_cur % 2 == 0)
        {
            n_cur /= 2;
        }
        else
        {
            n_cur = 3 * n_cur + 1;
        }

        count++;
    }

    collatzValues[n] = count;

    for (unsigned long long i : updatedIndices)
    {
        if (i < collatzValues.size() && collatzValues[i] == 0)
        {
            collatzValues[i] = count;
        }
        count--;
    }
}

void returnHighestCollatz(const vector<unsigned long long> &collatzValues, const unsigned long long &n)
{
    unsigned long long max = 1;
    unsigned long long maxIdx = 1;
    for (unsigned long long i = 1; i <= n; i++)
    {
        if (collatzValues[i] >= max)
        {
            max = collatzValues[i];
            maxIdx = i;
        }
    }
    cout << maxIdx << endl;
}

int main()
{
    int t;
    cin >> t;
    unsigned long long index = 1;
    vector<unsigned long long> collatzValues(1000, 0);

    while (t--)
    {
        unsigned long long n;
        cin >> n;

        if (index < n)
        {
            if (n >= collatzValues.size())
                collatzValues.resize(n + 1, 0);

            for (unsigned long long i = index; i <= n; i++)
            {
                countCollatz(collatzValues, i);
            }
            index = n;
        }

        returnHighestCollatz(collatzValues, n);
    }

    return 0;
}
