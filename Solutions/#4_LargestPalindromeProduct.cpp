#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

bool isPalindrome(unsigned long long possiblePalindrome)
{
    string palindromeString = std::to_string(possiblePalindrome);
    for (int i = 0; i < palindromeString.size() / 2; i++)
    {
        if (!(palindromeString[i] == palindromeString[palindromeString.size() - i - 1]))
            return false;
    }
    return true;
}

unsigned long long palindromeN(std::vector<unsigned long long> &palindromeLst, int &n)
{
    int index = 0;
    if (palindromeLst.size() == 1)
        return palindromeLst[0];

    while (palindromeLst[index + 1] < n)
    {
        cout << palindromeLst[index] << endl;
        index += 1;
    }
    return palindromeLst[index];
}

unsigned long long findBiggestPalindrome(int n)
{
    for (int i = n - 1; i >= 101101; --i)
    {
        if (isPalindrome(i))
        {
            // find 3 digit j that divides i
            for (int j = 999; j >= 100; --j)
            {
                // check if i is divisible by j and the result is a 3 digit number
                if (i % j == 0 && i / j < 1000)
                {
                    return i;
                }
            }
        }
    }
    return -1;
}

int main()
{
    string t_temp;
    getline(cin, t_temp);

    int t = stoi(t_temp);

    for (int i = 0; i < t; i++)
    {
        string n_temp;
        getline(cin, n_temp);

        int n = stoi(n_temp);

        cout << findBiggestPalindrome(n) << endl;
    }

    return 0;
}
