#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);

void FibSum(long &n)
{
    long a = 0, b = 1, c = 0, sum = 0;

    // Iterate until Fibonacci number is less than n
    while (c < n)
    {
        c = a + b;
        a = b;
        b = c;

        // If the Fibonacci number is even, add it to the sum
        if (c % 2 == 0 && c < n)
        {
            sum += c;
        }
    }

    // Print the sum of even Fibonacci numbers
    cout << sum << endl;
}

int main()
{
    string t_temp;
    getline(cin, t_temp);

    int t = stoi(ltrim(rtrim(t_temp)));

    for (int t_itr = 0; t_itr < t; t_itr++)
    {
        string n_temp;
        getline(cin, n_temp);

        long n = stol(ltrim(rtrim(n_temp)));

        FibSum(n);
    }
    return 0;
}

string ltrim(const string &str)
{
    string s(str);

    s.erase(
        s.begin(),
        find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));

    return s;
}

string rtrim(const string &str)
{
    string s(str);

    s.erase(
        find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
        s.end());

    return s;
}
