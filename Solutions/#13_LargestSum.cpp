#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

string sumString(const string &no1, const string &no2)
{
    int len1 = no1.length();
    int len2 = no2.length();
    int carry = 0;
    int maxLength = max(len1, len2);
    string result(maxLength + 1, '0'); // Potencial Carry
    int resultIndex = maxLength;

    for (int i = len1 - 1, j = len2 - 1; i >= 0 || j >= 0 || carry > 0; --i, --j, --resultIndex)
    {
        int digit1 = (i >= 0) ? (no1[i] - '0') : 0;
        int digit2 = (j >= 0) ? (no2[j] - '0') : 0;
        int sum = digit1 + digit2 + carry;
        carry = sum / 10;
        result[resultIndex] = '0' + (sum % 10);
    }

    return result.substr(resultIndex + 1);
}

int main()
{
    string line;
    getline(cin, line);
    int t = stoi(line);

    getline(cin, line);
    string sum = line;

    for (int i = 1; i < t; i++)
    {
        string t_line;
        getline(cin, t_line);
        sum = sumString(t_line, sum);
    }

    cout << sum.substr(0, 10);
    return 0;
}
