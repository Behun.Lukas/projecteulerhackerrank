#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);
vector<string> split(const string &);

unsigned long long getMaxProduct(const vector<vector<int>> &grid)
{
    unsigned long long max = 0;

    // horizontal check
    for (int i = 0; i < 20; i++)
    {
        for (int j = 0; j < 17; j++)
        {
            unsigned long long curr = grid[i][j] * grid[i][j + 1] * grid[i][j + 2] * grid[i][j + 3];
            if (curr > max)
            {
                max = curr;
            }
        }
    }

    // Vertical check
    for (int i = 0; i < 17; i++)
    {
        for (int j = 0; j < 20; j++)
        {
            unsigned long long curr = grid[i][j] * grid[i + 1][j] * grid[i + 2][j] * grid[i + 3][j];
            if (curr > max)
            {
                max = curr;
            }
        }
    }

    // diagonal lu->rd
    for (int i = 0; i < 17; i++)
    {
        for (int j = 0; j < 17; j++)
        {
            unsigned long long curr = grid[i][j] * grid[i + 1][j + 1] * grid[i + 2][j + 2] * grid[i + 3][j + 3];
            if (curr > max)
            {
                max = curr;
            }
        }
    }
    // diagonal ru->ld
    for (int i = 3; i < 20; i++)
    {
        for (int j = 0; j < 17; j++)
        {
            unsigned long long curr = grid[i][j] * grid[i - 1][j + 1] * grid[i - 2][j + 2] * grid[i - 3][j + 3];
            if (curr > max)
            {
                max = curr;
            }
        }
    }
    return max;
}

int main()
{

    vector<vector<int>> grid(20);

    for (int i = 0; i < 20; i++)
    {
        grid[i].resize(20);

        string grid_row_temp_temp;
        getline(cin, grid_row_temp_temp);

        vector<string> grid_row_temp = split(rtrim(grid_row_temp_temp));

        for (int j = 0; j < 20; j++)
        {
            int grid_row_item = stoi(grid_row_temp[j]);

            grid[i][j] = grid_row_item;
        }
    }
    cout << getMaxProduct(grid);
    return 0;
}

string ltrim(const string &str)
{
    string s(str);

    s.erase(
        s.begin(),
        find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));

    return s;
}

string rtrim(const string &str)
{
    string s(str);

    s.erase(
        find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
        s.end());

    return s;
}

vector<string> split(const string &str)
{
    vector<string> tokens;

    string::size_type start = 0;
    string::size_type end = 0;

    while ((end = str.find(" ", start)) != string::npos)
    {
        tokens.push_back(str.substr(start, end - start));

        start = end + 1;
    }

    tokens.push_back(str.substr(start));

    return tokens;
}
