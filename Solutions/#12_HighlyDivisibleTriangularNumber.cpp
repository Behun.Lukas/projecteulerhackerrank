#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int numberOfFactors(const int &n)
{
    int count = 0;
    for (int i = 1; i <= sqrt(n); i++)
    {
        if (n % i == 0)
        {
            count++;
            if (i != n / i)
            {
                count++;
            }
        }
    }
    return count;
}

bool compareTriangles(const pair<unsigned long long, unsigned long long> &a, const pair<unsigned long long, unsigned long long> &b)
{
    if (a.second != b.second)
    {
        return a.second < b.second;
    }
    else
    {
        return a.first < b.first;
    }
}

void insertTriangle(vector<pair<unsigned long long, unsigned long long>> &triangles, const pair<unsigned long long, unsigned long long> &newTriangle)
{
    auto it = lower_bound(triangles.begin(), triangles.end(), newTriangle, compareTriangles);
    triangles.insert(it, newTriangle);
}

void triangleNumber(const unsigned long long &n, vector<pair<unsigned long long, unsigned long long>> &triangles, unsigned long long &maxFactors, unsigned long long &maxTriangle)
{
    while (n >= maxFactors)
    {
        unsigned long long newN = maxTriangle + triangles.size() + 1;
        unsigned long long newNFact = numberOfFactors(newN);
        triangles.push_back(make_pair(newN, newNFact));
        maxTriangle = newN;
        if (newNFact > maxFactors)
        {
            maxFactors = newNFact;
        }
    }

    for (const auto &triangle : triangles)
    {
        if (triangle.second > n)
        {
            cout << triangle.first << endl;
            break;
        }
    }
}

int main()
{
    string line;
    getline(cin, line);
    int t = stoi(line);

    vector<pair<unsigned long long, unsigned long long>> triangles;
    triangles.push_back(make_pair(1, 1));
    unsigned long long maxFactors = 1;
    unsigned long long maxTriangle = 1;

    for (int i = 0; i < t; i++)
    {
        getline(cin, line);
        unsigned long long n = stoi(line);
        triangleNumber(n, triangles, maxFactors, maxTriangle);
    }
    return 0;
}
