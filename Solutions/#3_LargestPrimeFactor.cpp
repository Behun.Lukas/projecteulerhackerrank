#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);

void returnLargestPrime(unsigned long long &n)
{
    std::set<unsigned long long> primes;
    // checks all even primes
    while (n % 2 == 0)
    {
        primes.emplace(2);
        n = n / 2;
    }

    // checks all odd primes
    for (int i = 3; i <= sqrt(n); i = i + 2)
    {
        while (n % i == 0)
        {
            primes.emplace(i);
            n = n / i;
        }
    }

    // remaining n is prime
    if (n > 2)
    {
        primes.emplace(n);
    }
    std::vector<unsigned long long> primesVector(primes.begin(), primes.end());
    std::sort(primesVector.begin(), primesVector.end());
    cout << primesVector.back() << endl;
}

int main()
{
    string t_temp;
    getline(cin, t_temp);

    int t = stoi(ltrim(rtrim(t_temp)));

    for (int t_itr = 0; t_itr < t; t_itr++)
    {
        string n_temp;
        getline(cin, n_temp);

        unsigned long long n = stol(ltrim(rtrim(n_temp)));

        returnLargestPrime(n);
    }

    return 0;
}

string ltrim(const string &str)
{
    string s(str);

    s.erase(
        s.begin(),
        find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));

    return s;
}

string rtrim(const string &str)
{
    string s(str);

    s.erase(
        find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
        s.end());

    return s;
}
