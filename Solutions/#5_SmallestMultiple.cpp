#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int gcd(int a, int b)
{
    while (b != 0)
    {
        int temp = b;
        b = a % b;
        a = temp;
    }
    return a;
}

unsigned long long lcm(int a, int b)
{
    return (unsigned long long)a / gcd(a, b) * b;
}

unsigned int smallestMultiple(int n)
{
    unsigned int smallest = 1;
    for (int i = 2; i <= n; ++i)
    {
        smallest = lcm(smallest, i);
    }
    return smallest;
}

int main()
{
    string line;
    getline(cin, line);
    int t = stoi(line);

    for (int i = 0; i < t; i++)
    {
        string t_line;
        getline(cin, t_line);
        int n = stoi(t_line);
        cout << smallestMultiple(n) << endl;
    }

    return 0;
}
