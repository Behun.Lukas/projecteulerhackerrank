#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

bool isPrime(unsigned long long num)
{
    if (num <= 1)
        return false;
    if (num <= 3)
        return true;
    if (num % 2 == 0 || num % 3 == 0)
        return false;
    for (unsigned long long i = 5; i * i <= num; i += 6)
    {
        if (num % i == 0 || num % (i + 2) == 0)
            return false;
    }
    return true;
}

void generatePrimes(vector<unsigned long long> &primes, unsigned long long n, unsigned long long &largest)
{
    unsigned long long curr = largest;
    while (largest < n)
    {
        curr++;
        while (!isPrime(curr))
        {
            curr++;
        }
        primes.push_back(curr);
        largest = curr;
    }
}

int main()
{
    int t;
    cin >> t;
    vector<unsigned long long> primes(1, 2);
    unsigned long long largest = 2;
    for (int a0 = 0; a0 < t; a0++)
    {
        int n;
        cin >> n;

        if (n > largest)
        {
            generatePrimes(primes, n, largest);
        }
        unsigned long long sum = 0;
        unsigned long long index = 0;
        while (index < primes.size() && primes[index] <= n)
        {
            sum += primes[index];
            index++;
        }
        cout << sum << endl;
    }
    return 0;
}