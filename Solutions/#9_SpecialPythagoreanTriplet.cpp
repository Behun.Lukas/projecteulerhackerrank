#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <tuple>
using namespace std;

long long maxProductPythagoreanTriplet(int num)
{
    long long res = -1;
    for (int a = 1; a <= num / 3; a++)
    {
        for (int b = a; b <= (num - a) / 2; b++)
        {
            int c = num - a - b;
            if (a * a + b * b == c * c)
            {
                long long curr = (long long)a * b * c;
                if (curr > res)
                {
                    res = curr;
                }
            }
        }
    }
    return res;
}

int main()
{
    int t;
    cin >> t;
    for (int a0 = 0; a0 < t; a0++)
    {
        int n;
        cin >> n;

        cout << maxProductPythagoreanTriplet(n) << endl;
    }
    return 0;
}