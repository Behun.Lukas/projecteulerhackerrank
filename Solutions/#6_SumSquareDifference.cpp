#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);

unsigned long long absoluteDifference(const int &n)
{
    unsigned long long sumSquare = ((unsigned long long)n * (n + 1)) / 2;
    sumSquare = sumSquare * sumSquare;

    unsigned long long squareSum = ((unsigned long long)n * (n + 1) * ((2 * n) + 1)) / 6;
    return sumSquare - squareSum;
}

int main()
{
    string t_temp;
    getline(cin, t_temp);

    int t = stoi(ltrim(rtrim(t_temp)));

    for (int t_itr = 0; t_itr < t; t_itr++)
    {
        string n_temp;
        getline(cin, n_temp);

        int n = stoi(ltrim(rtrim(n_temp)));

        cout << absoluteDifference(n) << endl;
    }

    return 0;
}

string ltrim(const string &str)
{
    string s(str);

    s.erase(
        s.begin(),
        find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));

    return s;
}

string rtrim(const string &str)
{
    string s(str);

    s.erase(
        find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
        s.end());

    return s;
}
